# TChunk - stores one "Terrain Chunk". When the terrain is chunked out,
# we use ~16x16 chunks to make updates faster.

var offset = null
var dimensions = null
var worlddata = null
var mesh = null
var mesh_instance = null
var decals_node = null
var material = null
var texset = null
var material_info = null
var surface_tool = null

var _already_built = false

# One shared mesh with multiple surfaces for terrain, or multiple?
var multi_instance_mode = false

var _my_seed

func _init(offset, dimensions):
    self.offset = offset
    self.dimensions = dimensions
    randomize()
    #seed(self.offset.x * 100000 + self.offset.y * 1000000000 + self.offset.z)
    self._my_seed = randi() % 1000000

    # Set new surface tool
    surface_tool = SurfaceTool.new()

func _remove_mesh(instance):
    instance.get_parent().remove_child(instance)
    if decals_node != null:
        decals_node.get_parent().remove_child(decals_node)
        decals_node = null

#
# Removes this tchunk mesh and decal node
func unload():
    _remove_mesh(mesh_instance)

func create_mesh(mesh_instance_example):
    multi_instance_mode = true
    mesh_instance = mesh_instance_example.duplicate()
    mesh_instance.set_mesh(Mesh.new())
    mesh = mesh_instance.get_mesh()
    mesh_instance_example.get_parent().add_child(mesh_instance)

func set_mesh(mesh):
    multi_instance_mode = false
    self.mesh = mesh

func setup_worlddata(worlddata, material, texset, material_info):
    self.material = material
    self.texset = texset
    self.worlddata = worlddata
    self.material_info = material_info # various material info keyed by ID

#
# Adds a tuft sprite in the given location
func add_tuft_decal(location, tex):
    var size = 16
    var sprite = Sprite3D.new()
    #var tex = ImageTexture.new()
    #tex.create_from_image(image, 0) # STORAGE_RAW
    sprite.set_texture(tex)
    # TODO tweak this
    sprite.set_translation(location + Vector3(0, 1, 0))
    if decals_node == null:
        decals_node = Spatial.new()
        mesh_instance.get_parent().add_child(decals_node)
    decals_node.add_child(sprite)
    sprite.set_pixel_size(1.0 / 16)
    sprite.set_flag(GeometryInstance.FLAG_BILLBOARD, true)

#
# Rebuilds the mesh for this Terrain Chunk
func rebuild():
    if _already_built:
        if multi_instance_mode:
            var original_mesh_instance = mesh_instance
            create_mesh(mesh_instance) # clears & refreshes mesh
            _remove_mesh(original_mesh_instance)
        else:
            print("VoxelTerrain.gd: ERROR: Not yet supporting refreshing surfaces.")
            return
    print(offset, " - rebuilding TChunk 1   - ", floor(OS.get_ticks_msec() / 100) / 10)

    var end = offset + dimensions
    surface_tool.begin(VisualServer.PRIMITIVE_TRIANGLES)
    #print(offset, " - rebuilding TChunk 2   - ", floor(OS.get_ticks_msec() / 100) / 10)
    for x in range(offset.x, end.x):
        for y in range(offset.y, end.y):
            for z in range(offset.z, end.z):
                var vector = Vector3(x, y, z)
                var type = worlddata.get_block_material(vector)
                var shape = worlddata.get_block(vector)
                if type == worlddata.EMPTY or shape == worlddata.EMPTY:
                    continue # No block to add here
                var mat_info = material_info[type]
                if not worlddata.is_block_exposed(vector):
                    continue # Block is surrounded and totally hidden
                var surroundings = worlddata.get_needed_cube_faces(vector)
                #print(offset, " - vector ", vector, surroundings)
                cube_at(vector, surroundings, type)
                continue # Skip decals

                ######### Decal logic
                if mat_info.has('decal_tuft_texture'):
                    # Ensure top is exposed
                    if not surroundings[2]: # TOP
                        continue
                    #  Only look at a half of locations
                    #if (x + y + z % 3) != 0:
                    #    continue
                    seed(index_of(x, y, z) + self._my_seed)
                    if randi() % 12 == 0:
                        # arbitrary thingie, need to tweak algo
                        add_tuft_decal(vector, mat_info['decal_tuft_texture'])
    #print(offset, " - rebuilding TChunk 3   - ", floor(OS.get_ticks_msec() / 100) / 10)

    _already_built = true
#
# Returns array index of given x, y and z value
func index_of(x, y, z):
    return int(x) + (int(z) * dimensions.x) + (int(y) * dimensions.z * dimensions.x)

func commit():
    surface_tool.commit(mesh)

func deferred_commit():
    surface_tool.call_deferred("commit", mesh)

func get_terrain_offset_for_type(type):
    return texset.get_uv_for(type)

func get_terrain_uv_coefficient():
    # e.g. return Vector2(0.111111, 0.33333333) # 1/9, 1/3
    return texset.uv_coefficient

# This stuff was inspired by the MIT licensed
# toger5/Godot-Voxel-Game-MineCraftClone
func cube_at(pos, needed_faces, type):
    #    needed_faces is structured x (left,right) y (up,down) z (front, back) [0,0,1,0,0,0]
    var dirs = [Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1)]
    for i in range(3):
        for k in range(2):
            if needed_faces[i*2+k] != 0:
                var n = dirs[i]*-(k*2 -1)
                face_at(pos, n, type)

func face_at(pos,normal,type):
    var s = 0.50 # normally: 0.5
    pos += normal * s

    var vertices = [
        Vector3(0,  s, -s),
        Vector3(0, -s, -s),
        Vector3(0,  s,  s),
        Vector3(0, -s,  s)
    ]

    # Flip normals
    if normal.x + normal.y + normal.z < 0:
        vertices = [vertices[0], vertices[2], vertices[1], vertices[3]]

    # A list with the order of how often coordinates x, y, z will be exchanged
    var normal_list = [normal.x, normal.z, normal.y]
    for i in normal_list:
        if abs(i) > 0:
            break
        var index = 0
        for v in vertices:
            var v2 = v
            v.x = v2.y
            v.y = v2.z
            v.z = v2.x
            vertices[index] = v
            index += 1

    # Gets UV Offset info based on which material this obj is:
    var order_uv  = [[1, 0], [1, 1], [0, 1], [0, 1], [0, 0], [1, 0]]
    var uv_offset = get_terrain_offset_for_type(type)
    var uv_size   = get_terrain_uv_coefficient()
    var order_v   = [2, 0, 1, 1, 3, 2]

    for v in range(6):
        surface_tool.set_material(material)
        var uv = uv_offset + Vector2(order_uv[v][0], order_uv[v][1])
        surface_tool.add_uv(uv * uv_size)
        surface_tool.add_normal(normal)
        surface_tool.add_vertex(vertices[order_v[v]] + pos)
