When a block changes in any way (added or removed):

1. Look at affected TChunks

2. Generate new ones

3. Swap TChunks. Eventually, cross-fade if possible.

