WIP - nothing to see here!

# godot-voxview

Leverages my other godot libraries to view voxel-like terrain built
with voxmod.

This is generally just made for my own games, but I'm releasing it
freely so it can be used for other folks if their uses happen to align
with mine! That said, keeping documentation at top quality is not a
high priority for me.

## requirements

This depends on two of my other modules, `godot-voxtools` and
`godot-voxmod`.

To test it requires `godot-pannable-camera`

See the README on those on how to set up dependencies the way I do.

## VoxelTerrain

NOT IMPLEMENTED YET, but the ideal interface:

* `load_terrain_data(id, d3d_material, d3d_shape)` - Adds terrain data (to be
  associated with given ID, if unloading is necessary).

* `unload_terrain_data(id)` - Unloads given terrain data

* `delete_block(x, y, z)` - Deletes the given block

* `add_block(x, y, z, material, shape)` - Adds a new block in the given
  location with the given material and shape

* `set_perspective_location(v3_location)` - Given a Vector3 in universal space,
  sets the "perspective location", meaning roofs above this area are removed.
  TODO: Need to invent and implement roof-finding algo


