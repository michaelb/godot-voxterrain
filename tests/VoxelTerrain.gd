extends "./unittest.gd"

# Loads VoxelTerrain as a script only, not as a packed scene
const VoxelTerrain = preload('../VoxelTerrain.gd');
const Utils = preload('../Utils.gd');

var voxel_terrain = null
var entity_manager = null

const ModLoader = preload('../godot-voxmod/ModLoader.gd')

var modloader = null

func _get_offsets(x, y, z):
    var dimensions = Vector3(4, 4, 4)
    return Utils.get_offsets_to_refresh(Vector3(x, y, z), dimensions)

func tests():
    modloader = ModLoader.new()
    modloader.load_dir('../godot-voxmod/tests/data/testmod1')
    var voldata = modloader.vol_datas.values()[0]

    # Doesn't test much, basically just a smoke test
    testcase('Instantiates with data')
    voxel_terrain = VoxelTerrain.new()
    # add_child(voxel_terrain)
    voxel_terrain.set_mod(modloader)
    assert_ne(voxel_terrain.material, null, 'material was set')
    #assert_eq(voxel_terrain.tchunks_by_offset.size(), 16, 'generated 16 terrain chunks')

    # Unfortunately, the following is hard to test headlessly:
    #voxel_terrain.load_voldata(voldata)
    endcase()

    testcase('Utils.get_offsets_to_refresh')
    var ex
    ex = [Vector3(0, 0, 0)]
    assert_array_equal(ex, _get_offsets(0, 0, 0), 'origin corner')
    assert_array_equal(ex, _get_offsets(1, 1, 1), 'center')
    assert_array_equal(ex, _get_offsets(1, 1, 2), 'center')
    assert_array_equal(ex, _get_offsets(0, 1, 1), 'center')
    ex = [Vector3(0, 0, 0), Vector3(0, 0, 4)]
    assert_array_equal(ex, _get_offsets(0, 1, 3), 'above edge')
    ex = [Vector3(0, 0, 0), Vector3(0, 4, 0), Vector3(0, 0, 4)]
    assert_array_equal(ex, _get_offsets(0, 3, 3), 'above edge')
    endcase()
