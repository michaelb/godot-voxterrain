extends "./unittest.gd"

const SchemaConf = preload("../SchemaConf.gd")

func tests():
    var new_one = SchemaConf.new()
    var basedir = self.get_script().get_path().get_base_dir()
    var path = basedir.plus_file('data/test_schemaconf_0.cfg')
    var data = new_one.open(path)

    testcase('Reads in basic file')
    assert_array_equal(data.keys(), ['TILE', 'DATA'], 'top level keys are as expected')
    assert_eq(data['DATA'].size(), 1, 'data has one entry')
    var EXPECTED_DATA = {
        max_z = '61',
        max_x = '64',
        max_y = '9',
        path = 'test.data',
    }
    assert_dict_equal(data['DATA'][0], EXPECTED_DATA, 'DATA entry is as expected')

    assert_eq(data['TILE'].size(), 2, 'TILE has two entries')
    var EXPECTED_TILE = {
        id = '0',
        empty = 'true',
        color = 'white',
    }
    assert_dict_equal(data['TILE'][0], EXPECTED_TILE, 'TILE first entry is right')
    endcase()

