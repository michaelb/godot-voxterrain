const VoxelTerrain = preload('../VoxelTerrain.tscn')
const EditCursor = preload('../EditCursor.tscn')

var voxel_terrain = null
var entity_manager = null

const ModLoader = preload('../godot-voxmod/ModLoader.gd')

var modloader = null
var thread = null

onready var camera = get_node('PannableCamera')
func _ready():
    randomize()
    modloader = ModLoader.new()
    modloader.load_dir('../godot-voxmod/tests/data/testmod1')
    var voldata = modloader.vol_datas.values()[0]

    # Set up voxel terrain and entity manager
    voxel_terrain = VoxelTerrain.instance()
    voxel_terrain.set_mod(modloader)

    add_child(voxel_terrain)

    # Generates new world
    var world_data = modloader.generate_world()
    voxel_terrain.set_worldata(world_data)
    #voxel_terrain.load_voldata(voldata)
    voxel_terrain.set_name('VoxelTerrain')

    # Now create an edit cursor
    var edit_cursor = EditCursor.instance()
    edit_cursor.set_camera(camera)
    edit_cursor.add_to_terrain(voxel_terrain)
    add_child(edit_cursor)

    # Move the camera to 5, 5
    #camera.target = Vector3(5, 1, 5)

    thread = Thread.new()
    thread.start(self, "perpetually_adjusting", null, 0)

func perpetually_adjusting(u):
    # Thread based
    while true:
        # Finally update the results
        #voxel_terrain.update_mesh(camera.target, 32)
        #voxel_terrain.update_mesh(camera.target, 128)
        #voxel_terrain.update_mesh(camera.target, 32)
        voxel_terrain.update_mesh(camera.target, 1)
        print("updated mesh")
        OS.delay_msec(1000)

func hook_connections(u):
    # Have it be based on hooked connection
    camera.connect('moved', self, 'adjust_simulation_center')

func adjust_simulation_center():
    # Refresh based on camera center, minimum (1 radius, thus 1 tch)
    voxel_terrain.update_mesh(camera.target, 32)
