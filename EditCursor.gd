#
# A cursor that can be used for editing! By default it is controlled by
# mouse. Click to add a block, right click to remove.

const Consts = preload('../godot-voxtools/Consts.gd')

const MAX_ATTEMPTS = 100

onready var cst_sprite = get_node('TexSprite')

var voxel_terrain = null
var camera = null
var location = Vector3(0, 0, 0)
var materials_by_id = {}

var shape = Consts.CUBE
var material = 1
var material_index = 1

var texture_size = null

func _ready():
    set_process_input(true)
    var image_texture = voxel_terrain.modloader.texset.make_image_texture()
    texture_size = voxel_terrain.modloader.texset.size
    cst_sprite.set_texture(image_texture)
    cst_sprite.set_region_rect(calc_region_rect())
    cst_sprite.set_pixel_size(1.0/texture_size)
    float_to_surface()

func calc_region_rect():
    return Rect2(texture_size * 3 * material_index, 1, texture_size, texture_size)

func set_camera(camera):
    self.camera = camera

func add_to_terrain(voxel_terrain):
    self.voxel_terrain = voxel_terrain
    self.materials_by_id = voxel_terrain.modloader.materials_by_id

func float_to_surface():
    var voldata = self.voxel_terrain.voldata
    var attempts_left = MAX_ATTEMPTS
    var block = voldata.get_block(location)
    while block != Consts.EMPTY and attempts_left > 0:
        location.y += 1
        attempts_left -= 1
        block = voldata.get_block(location)
    _refresh_location()

func move_to_screen_position(mouse_pos):
    var voldata = self.voxel_terrain.voldata
    var ray = camera.project_ray_normal(mouse_pos)

    # Offset origin since it looks better when rounded this way
    var origin = camera.get_translation() + Vector3(0.5, 0, 0)

    var new_location = voldata.trace_ray_collision(origin, ray)
    if new_location == null:
        return false # Couldn't click anywhere
    location = new_location
    float_to_surface()
    _refresh_location()
    return true

func add_block():
    print("Adding block at: ", location, ' with material ', material)
    voxel_terrain.add_block(location, shape, material)
    float_to_surface()
    _refresh_location()

func remove_block():
    # Removes the block UNDER the cursor
    var remove_location = location + Vector3(0, -1, 0)
    if remove_location.y == 0:
        # Never remove at y = 0
        print("Can't remove base block")
        return
    print("Removing block at: ", remove_location)
    voxel_terrain.remove_block(remove_location)
    float_to_surface()
    _refresh_location()

func _refresh_location():
    # Moves to the location held by `location` variable
    var new_loc = self.voxel_terrain.get_translation()
    set_translation(new_loc + location)

func _refresh_material():
    material = materials_by_id.keys()[material_index]
    print("changing mateiral", material)
    cst_sprite.set_region_rect(calc_region_rect())

func _input(ev):
    # Process clicks and mouse movement
    var success
    if ev.type == InputEvent.MOUSE_BUTTON:
        if ev.button_index == BUTTON_LEFT and ev.pressed:
            success = move_to_screen_position(ev.pos)
            if success:
                add_block()
        if ev.button_index == BUTTON_RIGHT and ev.pressed:
            success = move_to_screen_position(ev.pos)
            if success:
                remove_block()
        if ev.button_index == BUTTON_WHEEL_UP:
            material_index = (material_index + 1) % materials_by_id.size()
            _refresh_material()
        if ev.button_index == BUTTON_WHEEL_DOWN:
            material_index = (material_index - 1) % materials_by_id.size()
            _refresh_material()

    elif ev.type == InputEvent.MOUSE_MOTION:
        move_to_screen_position(ev.pos) # todo: might be nice to throttle

    if ev.type != InputEvent.KEY:
        return

    #### XXX HACK, needs to be cleaned up
    # Subsequent checks are only for keys
    var scancode = ev.scancode
    if scancode == KEY_1:
        material_index = 0
    elif scancode == KEY_2:
        material_index = 1
    elif scancode == KEY_3:
        material_index = 2
    elif scancode == KEY_4:
        material_index = 3
    elif scancode == KEY_5:
        material_index = 4
    elif scancode == KEY_6:
        material_index = 5
    elif scancode == KEY_7:
        material_index = 6
    elif scancode == KEY_8:
        material_index = 7
    elif scancode == KEY_9:
        material_index = 8
    elif scancode == KEY_C:
        material_index = 10
    elif scancode == KEY_V:
        material_index = 11
    elif scancode == KEY_B:
        material_index = 12
    elif scancode == KEY_N:
        material_index = 13
    elif scancode == KEY_M:
        material_index = 14

    if material_index < 0:
        material_index = 0
    elif material_index >= materials_by_id.size():
        material_index = materials_by_id.size() - 1
    _refresh_material()


