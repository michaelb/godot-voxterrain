# Represents a voxel-ly encoded terrain system, along with entities
const Physics = preload('../godot-voxtools/physics/Physics.gd');
const TChunk = preload('./TChunk.gd');
const Utils = preload('./Utils.gd');
var material = null

var voldata = null

# True if we are looking at an infini world, False if we are loading a single
# voldata
var is_infinite_world = false

var helper = null
var physics = null
var mesh_instance = null
var modloader = null

var editable = false # unused

# TChunk dimensions
const TCHUNK_D = Vector3(16, 16, 16)
var tchunks_by_offset = {} # store all tchunks here

func set_mod(modloader):
    self.modloader = modloader
    material = modloader.texset.make_fixed_material()

func load_voldata(voldata):
    is_infinite_world = false
    self.voldata = voldata

func set_worldata(worldata):
    is_infinite_world = true
    self.voldata = worldata

func set_pattern(pattern):
    is_infinite_world = false
    self.voldata = pattern

func load_entities(entity_manager):
    for entity in entity_manager.get_entities():
        pass

func _ready():
    # Set up all the surfaces
    self.mesh_instance = get_node('TerrainMeshInstance')

#
# Needs to be manually called to display
var _last_start = null
var _last_end = null
func update_mesh(center, load_radius):
    if not is_infinite_world:
        print("VoxelTerrain.gd: WARNING: Attempting to call update_mesh on finite world")

    var r_vec = Vector3(load_radius, load_radius, load_radius)
    var start = ((center - r_vec) / TCHUNK_D).floor()
    var end = ((center + r_vec) / TCHUNK_D).ceil()

    # Skip if its the same
    if start == _last_start and end == _last_end:
        return

    var required_offsets = {}
    for x in range(start.x, end.x):
        for y in range(start.y, end.y):
            for z in range(start.z, end.z):
                var offset = Vector3(x, y, z)
                required_offsets[offset] = true
                if tchunks_by_offset.has(offset):
                    continue # Already loaded, do nothing

                # Create the tchunk necessary
                var position_offset = offset * TCHUNK_D
                var tchunk = new_tchunk(offset, position_offset)
                tchunk.rebuild()
                tchunk.commit()

    # Now, we loop through and unload ones we don't need anymore
    # TODO: should mark for deletion, then do sweep later, so we don't
    # thrash loading/unloading when next to an edge
    for offset in tchunks_by_offset:
        continue # XXX
        if not required_offsets.has(offset):
            var tchunk = tchunks_by_offset[offset]
            tchunk.unload()
            tchunks_by_offset.erase(offset)

    # Remember so we don't repeat the logic too much
    _last_start = start
    _last_end = end

func new_tchunk(offset, position_offset):
    var tchunk = TChunk.new(position_offset, TCHUNK_D)
    # tchunk.set_mesh(mesh_instance.get_mesh())
    tchunk.create_mesh(mesh_instance)
    tchunk.setup_worlddata(voldata, material, modloader.texset, modloader.materials_by_id)
    tchunks_by_offset[offset] = tchunk
    return tchunk

func get_tchunk_dimensions():
    var max_dim = voldata.get_world_dimension_vector()
    var tchunk_dim = max_dim / TCHUNK_D
    return tchunk_dim.ceil()

func render_all():
    if is_infinite_world:
        print("VoxelTerrain.gd: ERROR: Attempting to render_all on infinite world")
    var tc_dim = get_tchunk_dimensions()
    for x in range(0, tc_dim.x):
        for y in range(0, tc_dim.y):
            for z in range(0, tc_dim.z):
                var offset = Vector3(x, y, z)
                var position_offset = offset * TCHUNK_D
                var tchunk = new_tchunk(offset, position_offset)
                tchunk.rebuild()
                tchunk.commit()

#
# Refreshes the tchunk associated with the given region
func refresh_block(location):
    var position_offsets = Utils.get_offsets_to_refresh(location, TCHUNK_D)
    for p_offset in position_offsets:
        var offset = (p_offset / TCHUNK_D).floor()
        if not tchunks_by_offset.has(offset):
            print("WARNING: VoxelTerrain.gd: Does not have " + str(location))
            continue
        # Actual do rebuilding
        print('rebuilding tchunk', offset)
        tchunks_by_offset[offset].rebuild()
        tchunks_by_offset[offset].commit()

#
# After loaded, add an additional block, refresh relevant TChunks
func add_block(location, shape, material):
    var vector = location.floor()
    voldata.set_block(vector, shape, material)
    refresh_block(vector)
    # v--- need to reconsider physics refresh
    # physics.add_block(vector)

#
# After loaded, delete a block, refresh relevant TChunks
func remove_block(location):
    var vector = location.floor()
    voldata.delete_block(vector)
    refresh_block(vector)
    # v--- need to reconsider physics refresh
    #physics.reload_from_world(voldata)

