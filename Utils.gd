#
# Returns the TChunks that need to be refreshed if a change were to be made in
# the given location (with given tchunk dimensions). This is generally just the
# TChunk it was contained in, but may also be adjacent TChunks if its on a
# border.
static func get_offsets_to_refresh(location, tchunk_d):
    var tchunk_location = (location / tchunk_d).floor()
    var offsets = [tchunk_location]
    var inner = location - (tchunk_location * tchunk_d)

    # On X border
    if inner.x == 0:
        offsets.append(tchunk_location + Vector3(-1, 0, 0))
    elif inner.x == tchunk_d.x - 1:
        offsets.append(tchunk_location + Vector3(1, 0, 0))


    # On Y border
    if inner.y == 0:
        offsets.append(tchunk_location + Vector3(0, -1, 0))
    elif inner.y == tchunk_d.y - 1:
        offsets.append(tchunk_location + Vector3(0, 1, 0))

    # On Z border
    if inner.z == 0:
        offsets.append(tchunk_location + Vector3(0, 0, -1))
    elif inner.z == tchunk_d.z - 1:
        offsets.append(tchunk_location + Vector3(0, 0, 1))

    # Now clean up offsets, and multiply by the chunk size vector
    var clean_offsets = []
    for offset in offsets:
        if offset.x < 0 or offset.y < 0 or offset.z < 0:
            continue
        clean_offsets.append(offset * tchunk_d)
    return clean_offsets

